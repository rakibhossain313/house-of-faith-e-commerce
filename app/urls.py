from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from .import views

urlpatterns = [
    path('', views.HomeView.as_view(), name='home'),
    path('about-us/', views.AboutView.as_view(), name='aboutView'),
	path('contact/', views.ContactView.as_view(), name='contactView'),
	path('faqs/', views.FaqView.as_view(), name='faqView'),
    path('products/', include('products.urls')),
    path('account/', include('accounts.urls')),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('admin/', admin.site.urls),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

admin.site.site_header = "House Of Faith"
admin.site.site_title  = "House Of Faith Admin Portal"
