import os
import time
import datetime

from django.views import View
from django.conf import settings
from django.core import serializers
from django.http import HttpResponse
from django.shortcuts import render, redirect

from app_libs.dump import dd
from credentials.models import(
	Faq,
	CompanyInfo,
	ShopOpeningTime,
	OwnerAndTeam,
	Default_Slide,
	Carousel_Slide,
	Home_Credentials,
)
from products.models import(
	Product_Categories,
	Product_Sub_Categories,
	Product_Sub_Sub_Categories,
	Products,
)
from accounts.models import(
	User,
)


class HomeView(View):
	def get(self, request):
		data = {}
		data['home_title'] = 'house of faith'
		data['d_slides'] = Default_Slide.objects.all_D_Slide()
		data['c_slides'] = Carousel_Slide.objects.all_C_Slide()
		data['product_categories'] = Product_Categories.objects.get_all_category()
		data['cis'] = CompanyInfo.objects.all_ci()
		data['home'] = Home_Credentials.objects.all_home_cre()

		template = 'index.html'

		# dump_data = dd(request, data['home_credentials'])
		# return HttpResponse(dump_data)

		return render(request, template, data)


class AboutView(View):
	def get(self, request):
		data = {}
		data['page_title'] = 'about us'
		data['oats'] = OwnerAndTeam.objects.all_oat()
		data['cis'] = CompanyInfo.objects.all_ci()

		template = 'about-us.html'

		# dump_data = dd(request, data['cis'])
		# return HttpResponse(dump_data)

		return render(request, template, data)


class ContactView(View):
	def get(self, request):
		data = {}
		data['page_title'] = 'contact us'
		data['sots'] = ShopOpeningTime.objects.all_sot()
		data['cis'] = CompanyInfo.objects.all_ci()

		template = 'contact.html'

		return render(request, template, data)


class FaqView(View):
	def get(self, request):
		data = {}
		data['page_title'] = 'faqs'
		data['faqs'] = Faq.objects.all_faq()
		data['cis'] = CompanyInfo.objects.all_ci()

		template = 'faqs.html'

		# print(data['faqs'][0].answers)
		return render(request, template, data)
