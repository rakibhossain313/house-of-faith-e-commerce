from credentials.models import CompanyInfo
from products.models import(
    Product_Categories,
    Products,
    Product_Size,
    Product_Available_Stock,
)


def context_data(request):
    data = {}
    data['cis'] = CompanyInfo.objects.all_ci()
    data['sizes'] = Product_Size.objects.get_all_size()
    data['menus'] = Product_Categories.objects.get_all_category()
    data['categories'] = Product_Categories.objects.get_all_category()
    data['latest_products'] = Products.objects.get_latest_product()
    data['mother_title'] = 'house of faith'

    return data
