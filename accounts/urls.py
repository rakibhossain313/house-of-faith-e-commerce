from django.urls import path, include
from .import views

app_name = 'accounts'

urlpatterns = [
	path('', include('django.contrib.auth.urls')),
	path('', views.MyAccount.as_view(), name='myAccount'),
    path('login/', views.LoginView.as_view(), name='loginView'),
    # path('signin/', views.LoginView.as_view(), name='loginView'),
    path('signup/', views.Signup.as_view(), name='signup'),
]
