import os
import time
import datetime

from django.views import View
from django.views.generic import DetailView
from django.conf import settings
from django.core import serializers
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.contrib.auth.mixins import LoginRequiredMixin

from accounts.models import User
from .forms import NameForm


# Create your views here.
class LoginView(View):
	def get(self, request):
		data = {}
		data['page_title'] = 'login'
		template = 'registration/login.html'

		return render(request, template, data)


class Signup(View):

	def get(self, request):
		data = {}
		data['page_title'] = 'register'
		template = 'registration/register.html'

		return render(request, template, data)

	def post(self, request):
		# create a form instance and populate it with data from the request:
		form = NameForm(request.POST)

		# check whether it's valid:
		if form.is_valid():
			email = request.POST.get('email')
			password = request.POST.get('password')
			first_name = request.POST.get('first_name')
			last_name = request.POST.get('last_name')

			User.objects.create_user(email, password, first_name=first_name, last_name=last_name)
			user = authenticate(request, email=email, password=password)

			if user is not None:
				login(request, user)
				return redirect('home')
			else:
				return redirect('accounts:signup')

		return redirect('accounts:signup')


class MyAccount(LoginRequiredMixin, DetailView):
	template_name = 'accinfo/account.html'
	def get_object(self):
		return self.request.user
