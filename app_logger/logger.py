# import logging
# from datetime import datetime
#
#
# def get_client_ip(request):
#     x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
#     if x_forwarded_for:
#         ip = x_forwarded_for.split(',')[0]
#     else:
#         ip = request.META.get('REMOTE_ADDR')
#     return ip
#
#
# class ProductUploadLogger():
#
#     def __init__(self):
#         print("Product Upload Logger Calling")
#         self.logger = logging.getLogger('product_upload')
#
#     def create_log(self, request, data=None):
#         info = {
#             "ip": get_client_ip(request),
#             "time": datetime.now(),
#             "request_with": data
#         }
#         self.logger.debug("=============================================x================================================")
#         self.logger.debug(info)
