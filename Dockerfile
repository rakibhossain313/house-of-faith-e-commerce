FROM python:3.8-slim

# Creating working directory
RUN mkdir /app
WORKDIR /app

# Set timezone
RUN echo "Asia/Dhaka" > /etc/timezone

# Copy requirements files
COPY ./requirements.txt ./
RUN pip install -r requirements.txt

# Copy project files
COPY . .
