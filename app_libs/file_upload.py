import os
import time
import datetime
from io import BytesIO

from PIL import Image
from django.conf import settings
from django.http import HttpResponse
from django.core.files.storage import FileSystemStorage


class FileUpload():

	def upload(request, path, file):
		try:
			file_path = '/'+path+'/'+datetime.date.today().isoformat()
			fs = FileSystemStorage(location=settings.MEDIA_ROOT+file_path)
			tempname = str(time.time_ns())+'.png'

			filename = fs.save(tempname, file)

			file_original_path = file_path+'/'+str(filename)
			uploaded_file_url = fs.url(file_original_path)

			req_data = {'req_data': request, 'from': 'File Upload', 'message': 'Product image uploaded successfully'}

			return uploaded_file_url
		except IOError:
			req_data = {'req_data': request, 'from': 'File Upload', 'message': 'Error during product image upload'}

	def resizeUpload(request, path, image_file):

		try:
			file_path = '/'+path+'/'+datetime.date.today().isoformat()
			fs = FileSystemStorage(location=settings.MEDIA_ROOT+file_path)
			tempname = str(time.time_ns())+'.png'

			# ------------ image resize ------------
			size = 420, 420
			image_file = BytesIO(image_file.file.read())
			image = Image.open(image_file)
			image.thumbnail(size, Image.ANTIALIAS)
			image_file = BytesIO()
			image.save(image_file, 'PNG')
			# --------- end of image resize ---------

			filename = fs.save(tempname, image_file)

			file_original_path = file_path+'/'+str(filename)
			uploaded_file_url = fs.url(file_original_path)

			req_data = {'req_data': request, 'from': 'File Upload', 'message': 'Product image uploaded after resize successfully'}

			return uploaded_file_url
		except IOError:
			req_data = {'req_data': request, 'from': 'File Upload', 'message': 'Error during product image resize upload'}
