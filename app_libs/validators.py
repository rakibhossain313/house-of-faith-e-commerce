import re


def is_phone_valid(phone_number):
    """
    Check phone number is valid or not. @return: Boolean

    Old Regex: ^(?:\+?88)?01[15-9]\d{8}$
        - Except 013 and 014
    New Regex: ^(?:\+?88)?01[3-9]\d{8}$
        - Support: 013,014, and others mobile numbers
    """
    if phone_number:
        MOBILE_REGEX = re.compile('^(?:\+?88)?01[3-9]\d{8}$')
        if MOBILE_REGEX.match(phone_number):
            return True
        else:
            return False
    else:
        return False