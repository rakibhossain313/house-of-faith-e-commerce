from django.urls import path
from .import views

app_name = 'products'

urlpatterns = [
    path('add-to-wishlist/', views.addToWishlist.as_view(), name='addToWishlist'),
    path('get-product/', views.getProduct.as_view(), name='getProduct'),
    path('add-to-cart/', views.addToCart.as_view(), name='addToCart'),
    path('remove-from-cart/', views.removeFromCart.as_view(), name='removeFromCart'),
    path('place-order/', views.placeOrder.as_view(), name='placeOrder'),
    path('detail/<str:product_slug>/', views.ProductDetail.as_view(), name='productDetailView'),
    path('<str:category_slug>/', views.ProductView.as_view(), name='productView'),
    path('<str:category_slug>/<str:sub_category_slug>/', views.ProductView.as_view(), name='productView'),
    path('<str:category_slug>/<str:sub_category_slug>/<str:sub_sub_category_slug>/', views.ProductView.as_view(), name='productView'),
]
