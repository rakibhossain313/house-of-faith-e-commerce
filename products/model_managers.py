from django.db import models, transaction, IntegrityError
from django.core.exceptions import ObjectDoesNotExist
from django.utils.text import slugify
from django.db.models import F, Value


# Product Category Model manager
class ProductCategoryManager(models.Manager):
    # get all product category
    def get_all_category(self):
        return self.filter()

    # get product category
    def get_category(self, id):
        try:
            return self.get(id=id)
        except ObjectDoesNotExist:
            return False


# Product Sub-Category Model manager
class ProductSubCategoryManager(models.Manager):
    def get_all_sub_categories(self):
        return self.filter()

    # get sub-category
    def get_sub_category(self, id):
        try:
            return self.get(id=id)
        except ObjectDoesNotExist:
            return False

    # get category wise sub-category
    def get_category_wise_sub_category(self, id):
        try:
            return self.filter(product_category=id)
        except ObjectDoesNotExist:
            return False


# Product Sub-Category Model manager
class ProductSubSubCategoryManager(models.Manager):
    def get_all_sub_sub_categories(self):
        return self.filter()

    # get product category
    def get_sub_sub_category(self, id):
        try:
            return self.get(id=id)
        except ObjectDoesNotExist:
            return False

    # get category wise sub-category
    def get_sub_category_wise_sub_sub_category(self, id):
        try:
            return self.filter(product_sub_category=id)
        except ObjectDoesNotExist:
            return False


# Product Model manager
class ProductManager(models.Manager):
    # get all product
    def get_all_product(self, type=''):
        try:
            if type == 'admin':
                query = self.filter()
            else:
                query = self.filter(status=True)
            return query
        except ObjectDoesNotExist:
            return False

    # get categorical product
    def get_categorical_product(self, category_slug, sub_category_slug, sub_sub_category_slug):
        try:
            query = self.filter(status=True)
            if category_slug:
                query &= self.filter(product_category_slug=category_slug)
            if sub_category_slug:
                query &= self.filter(product_sub_category_slug=sub_category_slug)
            if sub_sub_category_slug:
                query &= self.filter(product_sub_sub_category_slug=sub_sub_category_slug)
            return query
        except ObjectDoesNotExist:
            return False

    # get product
    def get_product(self, id, type=''):
        try:
            if type == 'admin':
                query = self.get(id=id)
            else:
                query = self.get(id=id, status=True)
            return query
        except ObjectDoesNotExist:
            return False

    # get product by slug
    def get_product_by_slug(self, slug, type=''):
        try:
            if type == 'admin':
                query = self.get(slug=slug)
            else:
                query = self.get(slug=slug, status=True)
            return query
        except ObjectDoesNotExist:
            return False

    # get product detail
    def get_product_detail(self, product_slug):
        try:
            return self.get(slug=product_slug, status=True)
        except ObjectDoesNotExist:
            return False

    # save product
    def save(self, data):
        return self.create(
            title=data['title'],
            slug=slugify(data['slug']),
            product_category=data['product_category'],
            product_category_slug=slugify(data['product_category_slug']),
            product_sub_category=data['product_sub_category'],
            product_sub_category_slug=slugify(data['product_sub_category_slug']),
            product_sub_sub_category=data['product_sub_sub_category'],
            product_sub_sub_category_slug=slugify(data['product_sub_sub_category_slug']),
            status=data['status'],
            description=data['description']
        )

    # update product
    def update(self, id, data):
        sid = transaction.savepoint()

        try:
            product = self.get(id=id)

            product.title = data['title']
            product.slug = slugify(data['slug'])
            product.product_category = data['product_category']
            product.product_category_slug = slugify(data['product_category_slug'])
            product.product_sub_category = data['product_sub_category']
            product.product_sub_category_slug = slugify(data['product_sub_category_slug'])
            product.product_sub_sub_category = data['product_sub_sub_category']
            product.product_sub_sub_category_slug = slugify(data['product_sub_sub_category_slug'])
            product.status = data['status']
            product.description = data['description']

            product.save()

            transaction.savepoint_commit(sid)

            return product
        except IntegrityError as e:
            transaction.savepoint_rollback(sid)
            print(e.message)

    def change_product_stock_status(self, id, status):
        try:
            product = self.get(id=id)
            product.status = status
            product.save()
            return True
        except ObjectDoesNotExist:
            return False

    def delete(self, id):
        try:
            return self.filter(id=id).delete()
        except ObjectDoesNotExist:
            return False

    def get_latest_product(self):
        try:
            return self.filter().order_by('-id')[:6]
        except ObjectDoesNotExist:
            return False


# Product Available Stock manager
class ProductAvailableStockManager(models.Manager):
    # get all stock product
    def get_all_stock_product(self):
        try:
            query = self.filter()
            return query
        except ObjectDoesNotExist:
            return False

    # get stock product
    def get_stock_product(self, product_id='', category_id='', sub_category_id='', sub_sub_category_id='', type=''):
        try:
            query = self.filter(product=product_id)
            if category_id:
                query &= self.filter(product_category=category_id)

            if type == 'product' or type == 'product_offer':
                return query
            else:
                if sub_category_id:
                    query &= self.filter(product_sub_category=sub_category_id)
                if sub_sub_category_id:
                    query &= self.filter(product_sub_sub_category=sub_sub_category_id)
                return query
        except ObjectDoesNotExist:
            return False

    # get stock product
    def get_product_unit_price(self, product_id):
        try:
            query = self.get(product=product_id)
            return query.unit_price
        except ObjectDoesNotExist:
            return False

    # save stock product
    def save(self, data):
        return self.create(
            product=data['product'],
            product_category=data['product_category'],
            product_sub_category=data['product_sub_category'],
            product_sub_sub_category=data['product_sub_sub_category'],
            quantity=data['quantity'],
            unit_price=data['unit_price'],
            total_price=data['total_price']
        )

    # update stock product
    def update(self, id, data):
        sid = transaction.savepoint()

        try:
            stock = self.get(id=id)

            if 'new_qty' in data:
                stock.quantity = data['new_qty']
                stock.unit_price = data['new_unit_price']
                stock.total_price = data['new_total_price']
            elif 'amount' in data:
                stock.offer_amount = data['amount']
                stock.after_offer_unit_price = data['after_offer_unit_price']
                stock.after_offer_total_price = data['after_offer_total_price']
            else:
                stock.product_sub_category = data['product_sub_category']
                stock.product_sub_sub_category = data['product_sub_sub_category']

            stock.save()

            transaction.savepoint_commit(sid)

            return stock
        except IntegrityError as e:
            transaction.savepoint_rollback(sid)
            print(e.message)


class ProductSizeManager(models.Manager):
    # get all product size
    def get_all_size(self):
        return self.filter()

    # get product
    def get_product_size(self, id):
        try:
            return self.get(id=id)
        except ObjectDoesNotExist:
            return False


class ProductWiseSizeManager(models.Manager):
    # get product wise size
    def get_product_wise_size(self, id):
        try:
            return self.get(product=id)
        except ObjectDoesNotExist:
            return False

    # save product wise size
    def save(self):
        return self.create()

    # delete product wise size
    def delete(self, id):
        try:
            return self.get(product=id).delete()
        except ObjectDoesNotExist:
            return False


class ProductColorManager(models.Manager):
    # get all product size
    def get_all_color(self):
        return self.filter()

    # get product
    def get_product_color(self, id):
        try:
            return self.get(id=id)
        except ObjectDoesNotExist:
            return False


class ProductWiseColorManager(models.Manager):
    # get product wise color
    def get_product_wise_color(self, id):
        try:
            return self.get(product=id)
        except ObjectDoesNotExist:
            return False

    # save product wise color
    def save(self):
        return self.create()

    # delete product wise color
    def delete(self, id):
        try:
            return self.get(product=id).delete()
        except ObjectDoesNotExist:
            return False


class ProductOfferManager(models.Manager):
    # get all product offer
    def get_all_offer(self):
        return self.filter()

    # get product offer
    def get_offer(self, id):
        try:
            return self.get(id=id)
        except ObjectDoesNotExist:
            return False


class ProductWiseOfferManager(models.Manager):
    # get all product wise offer
    def get_all_product_wise_offer(self):
        try:
            return self.filter()
        except ObjectDoesNotExist:
            return False

    # get product wise offer
    def get_product_wise_offer(self, id):
        try:
            return self.get(id=id)
        except ObjectDoesNotExist:
            return False

    # get product wise offer availability
    def get_product_offer_availability(self, data):
        try:
            return self.get(product=data['product_id'], product_offer=data['product_offer_id'])
        except ObjectDoesNotExist:
            return False

    # save product wise offer
    def save(self, data):
        return self.create(
            product=data['product'],
            product_offer=data['product_offer'],
            amount=data['amount']
        )

    # delete product wise color
    def delete(self, id):
        try:
            return self.get(id=id).delete()
        except ObjectDoesNotExist:
            return False


class ProductImageManager(models.Manager):

    # get all product image
    def get_all_product_image(self):
        return self.filter()

    # get product image
    def get_product_image(self, id):
        try:
            return self.get(id=id)
        except ObjectDoesNotExist:
            return False

    def get_product_all_image(self, id):
        try:
            return self.filter(product=id)
        except ObjectDoesNotExist:
            return False

    # save product image
    def save(self, data):
        return self.create(product=data['product'], image_path=data['file_path'])

    def delete(self, id):
        try:
            return self.filter(id=id).delete()
        except ObjectDoesNotExist:
            return False


class OrderManager(models.Manager):
    # get order
    def get_all_order(self):
        try:
            return self.filter(is_complete=True)
        except ObjectDoesNotExist:
            return False

    # get order
    def get_order(self, id='', customer=''):
        try:
            if id:
                return self.get(id=id)
            return self.filter(customer=customer, is_complete=False).last()
        except ObjectDoesNotExist:
            return False

    # save product wise size
    def save(self, customer):
        return self.create(customer=customer)

    def update_order_status(self, id):
        with transaction.atomic():
            return self.filter(id=id).update(is_complete=True)

    def cancel_order(self, id):
        with transaction.atomic():
            return self.filter(id=id).update(is_complete=False)

    # delete product wise size
    def delete(self, id):
        try:
            return self.get(product=id).delete()
        except ObjectDoesNotExist:
            return False


class OrderItemManager(models.Manager):
    # get order
    def get_order_item(self, id):
        try:
            return self.get(order=id)
        except ObjectDoesNotExist:
            return False

    # save product wise size
    def save(self, data):
        return self.create(order=data['order'], product=data['product'], quantity=data['quantity'],
                           unit_price=data['unit_price'])

    # delete product wise size
    def delete(self, id):
        try:
            return self.get(id=id).delete()
        except ObjectDoesNotExist:
            return False

    def update_quantity(self, order_id, product_id, quantity):
        with transaction.atomic():
            return self.filter(order_id=order_id, product_id=product_id).update(quantity=quantity)


class ShippingAddressManager(models.Manager):

    def get_all_place_order(self):
        try:
            return self.filter(order__is_complete=True)
        except ObjectDoesNotExist:
            return False

    def get_shipping_address(self, id='', customer=''):
        try:
            if id:
                return self.get(id=id)
            return self.get(customer=customer)
        except ObjectDoesNotExist:
            return False

    # save
    def save(self, data):
        return self.create(**data)

    # delete
    def delete(self, id):
        try:
            return self.get(product=id).delete()
        except ObjectDoesNotExist:
            return False
