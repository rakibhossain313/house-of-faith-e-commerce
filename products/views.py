from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.forms import model_to_dict
from django.http import HttpResponse, JsonResponse
from django.core import serializers
from django.shortcuts import render, redirect
from django.views import View
from django.contrib import messages

from django.conf import settings
from django.core.mail import send_mail

from app_libs.file_upload import FileUpload
from products.forms import ProductForm

from app_libs.dump import dd
from accounts.models import User
from .models import (
    Products,
    Product_Image,
    Product_Categories, Product_Sub_Categories, Product_Sub_Sub_Categories, Product_Wise_Size, Product_Size,
    Product_Wise_Color, Product_Color, Product_Available_Stock, Order, OrderItem, ShippingAddress,
)


class ProductView(View):
    def get(self, request, category_slug='', sub_category_slug='', sub_sub_category_slug=''):
        data = {}

        data['page_title'] = category_slug.replace('-', ' ').capitalize() +' | '+ 'House Of Faith'
        if sub_category_slug:
            data['page_title'] = sub_category_slug.replace('-', ' ').capitalize() +' - '+ data['page_title']
            if sub_sub_category_slug:
                data['page_title'] = sub_sub_category_slug.replace('-', ' ').capitalize() +' - '+ data['page_title']

        data['menu_title_c'] = category_slug.replace('-', ' ').capitalize()
        data['menu_title_sc'] = sub_category_slug.replace('-', ' ').capitalize()
        data['menu_title_ssc'] = sub_sub_category_slug.replace('-', ' ').capitalize()

        request.session['category_slug'] = category_slug
        request.session['sub_category_slug'] = sub_category_slug
        request.session['sub_sub_category_slug'] = sub_sub_category_slug

        # pagination
        product_list = Products.objects.get_categorical_product(category_slug, sub_category_slug, sub_sub_category_slug)
        page = request.GET.get('page', 1)

        # dump_data = dd(request, product_list)
        # dump_data = dd(request, product_list[0].stock_product.all())
        # dump_data = dd(request, product_list[0].product_offer.all())
        # return HttpResponse(dump_data)

        # number of product per page
        paginator = Paginator(product_list, 15)
        try:
            data['products'] = paginator.page(page)
        except PageNotAnInteger:
            data['products'] = paginator.page(1)
        except EmptyPage:
            data['products'] = paginator.page(paginator.num_pages)
        # end of pagination

        template = 'products/collections.html'

        return render(request, template, data)


class ProductDetail(View):
    def get(self, request, product_slug):
        data = {}
        data['products'] = Products.objects.get_all_product()
        data['product_detail'] = Products.objects.get_product_detail(product_slug)
        template = 'products/product.html'

        # dump_data = dd(request, data['product_detail'])
        # dump_data = dd(request, data['product_detail'].stock_product.all())
        # dump_data = dd(request, data['product_detail'].product_wise_size.all())
        # return HttpResponse(dump_data)

        return render(request, template, data)


class addToCart(View):
    def get(self, request):
        data = {}
        data['page_title'] = 'cart'

        device = request.COOKIES['device']
        customer = User.objects.get_user(device=device)
        if request.user.is_authenticated:
            customer = User.objects.get_user(id=request.user.id)

        # get customer orders
        data['orders'] = Order.objects.get_order(customer=customer)

        # dump_data = dd(request, data['orders'].order_item.all())
        # return HttpResponse(dump_data)

        template = 'products/cart.html'
        return render(request, template, data)

    def post(self, request):
        user_id = request.POST.get('user_id')
        product_id = request.POST.get('product_id')
        quantity = request.POST.get('quantity')

        if user_id:
            customer = User.objects.get(id=user_id)
        else:
            device = request.COOKIES['device']
            customer, created = User.objects.get_or_create(device=device)

        product = Products.objects.get_product(id=product_id)
        unit_price = Product_Available_Stock.objects.get_product_unit_price(product_id=product_id)

        # create order
        order, created = Order.objects.get_or_create(customer=customer, is_complete=False)

        data = {}
        data['order'] = order
        data['product'] = product
        data['quantity'] = quantity
        data['unit_price'] = unit_price

        # create order item
        order_item = OrderItem.objects.save(data=data)

        if order_item:
            return HttpResponse(True)
        return HttpResponse(False)


class removeFromCart(View):
    def get(self, request):
        order_item_id = request.GET.get('order_item_id')
        delete_order_item = OrderItem.objects.delete(id=order_item_id)
        return HttpResponse(delete_order_item)


class placeOrder(View):
    def post(self, request):
        order_id = request.POST.get('order_id')
        product_ids = request.POST.getlist('product_id[]')
        quantities = request.POST.getlist('quantity[]')
        email = request.POST.get('email')
        phone = request.POST.get('phone')
        alternative_phone = request.POST.get('alternative_phone')
        address = request.POST.get('address')
        remarks = request.POST.get('remarks')

        if request.user.is_authenticated:
            customer = User.objects.get_user(id=request.user.id)
        else:
            device = request.COOKIES['device']
            customer = User.objects.get_user(device=device)

        order = Order.objects.get_order(customer=customer)

        data = {}
        data['email'] = email
        data['mobile_number'] = phone
        data['alternative_mobile_number'] = alternative_phone
        data['address'] = address
        data['remarks'] = remarks
        data['customer'] = customer
        data['order'] = order

        # subject = 'Order place receipt from House of faith'
        # message = f'Hi {email}, thank you for placing order in House of faith. Your order will be on your door soon.'
        # email_from = settings.EMAIL_HOST_USER
        # recipient_list = [email, ]
        # send_mail_status = send_mail(subject, message, email_from, recipient_list)
        #
        # if send_mail_status:
        #     # create place order
        #     _ = ShippingAddress.objects.save(data=data)
        #     _ = Order.objects.update_order_status(customer=customer)
        #     for key, value in enumerate(quantities):
        #         _ = OrderItem.objects.update_quantity(order_id=order_id, product_id=product_ids[key], quantity=quantities[key])
        #     messages.success(request, 'You placed order successfully!!')
        #     return redirect('/products/add-to-cart/')

        # create place order
        set_shipping_address = ShippingAddress.objects.save(data=data)
        if set_shipping_address:
            _ = Order.objects.update_order_status(id=order_id)
            for key, value in enumerate(quantities):
                _ = OrderItem.objects.update_quantity(order_id=order_id, product_id=product_ids[key], quantity=quantities[key])
            messages.success(request, 'You placed order successfully!!')
            return redirect('/products/add-to-cart/')

        messages.error(request, 'Some error occurred!!')
        return redirect('/products/add-to-cart/')


class addToWishlist(View):
    def post(self, request):
        product_slug = request.POST.get('product_slug')
        product = Products.objects.get_product_detail(product_slug)
        product_images = product.product_image.all()
        product_wise_sizes = product.product_wise_size.all()
        product_stock = product.stock_product.all()

        # dump_data = dd(request, product)
        # dump_data = dd(request, product_images)
        # return HttpResponse(dump_data)

        # product = serializers.serialize('json', [product])
        # return JsonResponse(product, safe=False)

        html = '<div class="modal-note wishlist-note">Added to <a href="wish-list.html">Wishlist</a></div>'
        html += '<div class="modal-body-inner">'
        html += '<div class="modal-left wishlist-left">'
        html += '<div class="modal-product wishlist-product">'
        html += '<div class="product-left">'
        html += '<div class="wishlist-image modal-image wishlist-image-213223800871">'
        html += '<img src="'+product_images[0].image_path+'" alt="product image">'
        html += '</div>'
        html += '</div>'
        html += '</div>'
        html += '</div>'
        html += '<div class="modal-right wishlist-right">'
        html += '<div class="wishlist-cart">'
        html += '<form action="#" method="post" class="variants-form variants"  enctype="multipart/form-data">'
        html += '<div class="form-left">'
        html += '<div class="product-right">'
        html += '<div class="name wishlist-name"><a href="">'+product.title+'</a></div>'
        if product.product_offer.all():
            html += '<div class="wishlist-price wishlist-price-213223800871"><span class="price"><span class="money" data-currency-usd="'+str(product_stock[0].after_offer_unit_price)+'" data-currency="BDT">৳ '+str(product_stock[0].after_offer_unit_price)+'</span></span>'
        else:
            html += '<div class="wishlist-price wishlist-price-213223800871"><span class="price"><span class="money" data-currency-usd="'+str(product_stock[0].unit_price)+'" data-currency="BDT">৳ '+str(product_stock[0].unit_price)+'</span></span>'
        html += '</div>'
        html += '</div>'
        html += '<div class="quantity-content">'
        html += '<label>QTY</label>'
        html += '<input type="hidden" id="max-quantity" value="'+str(product_stock[0].quantity)+'">'
        html += '<input type="text" size="5" class="item-quantity item-quantity-qs" name="quantity" value="1" oninput="maxQtyCheck($(this))">'
        html += '</div>'
        html += '</div>'
        html += '<div class="form-right">'
        html += '<div class="others-bottom">'
        html += '<a class="_btn btn-quick-shop" href="wish-list.html">View Wishlist</a>'
        html += '<a href="cart.html" class="_btn add-to-cart">Add to cart</a>'
        html += '</div>'
        html += '</div>'
        html += '</form>'
        html += '</div>'
        html += '</div>'
        html += '</div>'

        return HttpResponse(html)


class getProduct(View):
    def post(self, request):
        product_slug = request.POST.get('product_slug')
        product = Products.objects.get_product_detail(product_slug)
        product_images = product.product_image.all()
        product_wise_sizes = product.product_wise_size.all()
        product_stock = product.stock_product.all()

        # dump_data = dd(request, product)
        # dump_data = dd(request, product_images)
        # return HttpResponse(dump_data)

        # product = serializers.serialize('json', [product])
        # return JsonResponse(product, safe=False)

        html = '<div class="quick-shop-modal-bg" style="display: none;"></div>'
        html += '<div class="clearfix">'
        html += '<div class="col-md-6 product-image">'
        html += '<div id="quick-shop-image" class="product-image-wrapper">'
        html += '<div id="featuted-image" class="main-image featured">'
        html += '<img src="'+product_images[0].image_path+'" alt="product image">'
        html += '</div>'
        html += '</div>'
        html += '</div>'
        html += '<div class="col-md-6 product-information">'
        html += '<div id="quick-shop-container">'
        html += '<h3 id="quick-shop-title"><a href="">'+product.title+'</a></h3>'
        html += '<div class="rating-star"><span class="shopify-product-reviews-badge" data-id="6537875078"></span></div>'
        html += '<div class="quick-shop-management">'
        html += '<span class="management-title">Availability:</span>'
        if product_stock[0].quantity > 0:
            html += '<div class="management-description">In Stock</div>'
        else:
            html += '<div class="management-description">Out Of Stock</div>'
        html += '</div>'
        html += '<div class="description">'
        html += '<div id="quick-shop-description" class="text-left">'
        html += '<p>'+product.description+'</p>'
        html += '</div>'
        html += '</div>'
        html += '<form action="#" method="post" class="variants" id="quick-shop-product-actions" enctype="multipart/form-data">'
        if product_wise_sizes:
            html += '<div id="quick-shop-variants-container" class="variants-wrapper">'
            html += '<div class="selector-wrapper variant-wrapper-size">'
            html += '<label for="quick-shop-variants-213223800871-option-0">Size</label>'
            html += '<select class="single-option-selector" data-option="option1" id="quick-shop-variants-213223800871-option-0" required>'
            html += '<option value="">Select Product Size</option>'
            for product_wise_size in product_wise_sizes:
                for product_size in product_wise_size.product_size.all():
                    html += '<option value="'+str(product_size.id)+'">'+product_size.name+'</option>'
            html += '</select>'
            html += '</div>'
            html += '</div>'
        html += '<div id="quick-shop-price-container" class="product-price">'
        if product.product_offer.all():
            html += '<span class="price"><span class="money" data-currency-usd="'+str(product_stock[0].after_offer_unit_price)+'" data-currency="BDT">৳ '+str(product_stock[0].after_offer_unit_price)+'</span></span>'
        else:
            html += '<span class="price"><span class="money" data-currency-usd="'+str(product_stock[0].unit_price)+'" data-currency="BDT">৳ '+str(product_stock[0].unit_price)+'</span></span>'
        html += '</div>'
        html += '<div class="others-bottom">'
        html += '<div class="purchase-section">'
        html += '<div class="quantity-wrapper clearfix">'
        html += '<div class="wrapper">'
        html += '<input type="hidden" id="max-quantity" value="'+str(product_stock[0].quantity)+'">'
        html += '<input id="quantity" type="text" name="quantity" value="1" maxlength="5" size="5" class="item-quantity" oninput="maxQtyCheck($(this))">'
        html += '<div class="qty-btn-vertical">'
        html += '<span class="qty-down fa fa-chevron-down" title="Decrease" data-src="#quantity"></span>'
        html += '<span class="qty-up fa fa-chevron-up" title="Increase" data-src="#quantity"></span>'
        html += '</div>'
        html += '</div>'
        html += '</div>'
        html += '<div class="purchase">'
        html += '<button id="quick-shop-add" onclick="change_qs_quantity();" class="_btn add-to-cart" type="submit" name="add" style="opacity: 1;"><span class="cs-icon icon-cart"></span>Add to cart</button>'
        html += '</div>'
        html += '</div>'
        html += '<div class="comWish-content">'
        html += '<a title="Add To Wishlist" class="wishlist wishlist-extra-crispy-1" data-wishlisthandle="extra-crispy-1">'
        html += '<span class="icon-small icon-small-heart"></span>'
        html += '<span class="_compareWishlist-text">Wishlist</span>'
        html += '</a>'
        html += '<a title="Send email" class="send-email">'
        html += '<span class="icon-small icon-small-email"></span>'
        html += '<span class="_compareWishlist-text">Send email</span>'
        html += '</a>'
        html += '</div>'
        html += '</div>'
        html += '</form>'
        html += '<div class="supports-fontface">'
        html += '<span class="social-title">Share this</span>'
        html += '<div class="quick-shop-social">'
        html += '<a target="_blank" href="#" class="share-facebook">'
        html += '<span class="fa fa-facebook"></span>'
        html += '/a>'
        html += '<a target="_blank" href="#" class="share-twitter">'
        html += '<span class="fa fa-twitter"></span>'
        html += '</a>'
        html += '<a target="_blank" href="#" class="share-pinterest">'
        html += '<span class="fa fa-pinterest"></span>'
        html += '</a>'
        html += '<a target="_blank" href="#" class="share-google">'
        html += '<span class="fa fa-google-plus"></span>'
        html += '</a>'
        html += '</div>'
        html += '</div>'
        html += '</div>'
        html += '</div>'
        html += '</div>'

        return HttpResponse(html)
