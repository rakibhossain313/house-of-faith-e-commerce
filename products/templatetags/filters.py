import os
import time
import datetime

from django import template
from django.conf import settings
from django.core import serializers
from django.db.models import F
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.utils.safestring import mark_safe

from products.models import (
	Product_Wise_Size, 
	Product_Wise_Color, 
	Product_Image,
	Order,
	OrderItem,
	ShippingAddress
)

register = template.Library()


@register.filter(name='get_product_wise_size')
def get_product_wise_size(id):
	data = ''
	sizes = Product_Wise_Size.objects.get_product_wise_size(id)

	if sizes:
		data = mark_safe(', '.join('{product_size}'.format(
						product_size=size.name
					) for size in sizes.product_size.all()
		        )
		    )

	return data


@register.filter(name='get_product_wise_color')
def get_product_wise_color(id):
	data = ''
	colors = Product_Wise_Color.objects.get_product_wise_color(id)

	if colors:
		data = mark_safe(', '.join('{product_color}'.format(
						product_color=color.name
					) for color in colors.product_color.all()
		        )
		    )

	return data


@register.filter(name='get_product_image')
def get_product_image(id):
	data = ''
	images = Product_Image.objects.get_product_all_image(id)

	if images:
		data = mark_safe(''.join('<img src="{url}" alt="{alt}" width="{width}" height="{height}" />'.format(
						url=image.image_path,
						alt='product image',
						width=50,
						height=50,
		            ) for image in images
		        )
		    )

	return data


@register.filter(name='get_order_item')
def get_order_item(place_order):
	return place_order.order.order_item.annotate(product_title=F('product__title')).all()
