import os
import time
import requests
import json

from django.urls import path
from django.conf import settings
from django.core import serializers
from django.contrib import admin, messages
from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from app_libs.dump import dd
from app_libs.file_upload import FileUpload
from products.forms import ProductStockForm
from products.models import (
    Product_Categories,
    Product_Sub_Categories,
    Product_Sub_Sub_Categories,
    Product_Size,
    Product_Wise_Size,
    Product_Color,
    Product_Wise_Color,
    Product_Image,
    Products,
    Product_Available_Stock,
)


class AdminProductAvailableStock(admin.ModelAdmin):

    change_list_template = 'admin/stock/list.html'
    change_form_template = 'admin/stock/adjust.html'

    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            path('save/', self.save, name='adjust_stock'),
            path('get-product/', self.get_stock_product, name='get_stock_product'),
            path('get-sub-category/', self.get_sub_category, name='get_sub_category'),
            path('get-sub-sub-category/', self.get_sub_sub_category, name='get_sub_sub_category'),
        ]

        return custom_urls + urls

    def changelist_view(self, request, extra_context=None):
        data = extra_context or {}
        
        stock_products = Product_Available_Stock.objects.get_all_stock_product()
        page = request.GET.get('p', 1)

        # dump_data = dd(request, stock_products)
        # return HttpResponse(dump_data)

        # number of product per page
        paginator = Paginator(stock_products, 50)
        try:
            data['stock_products'] = paginator.page(page)
        except PageNotAnInteger:
            data['stock_products'] = paginator.page(1)
        except EmptyPage:
            data['stock_products'] = paginator.page(paginator.num_pages)
        # end of pagination

        return super().changelist_view(request, extra_context=data)

    def changeform_view(self, request, obj_id, form_url='', extra_context=None):
        data = extra_context or {}
        data['products'] = Products.objects.get_all_product(type='admin')
        data['product_categories'] = Product_Categories.objects.get_all_category()
        data['product_sub_categories'] = Product_Sub_Categories.objects.get_all_sub_categories()
        data['product_sub_sub_categories'] = Product_Sub_Sub_Categories.objects.get_all_sub_sub_categories()

        return super().changeform_view(request, obj_id, form_url, extra_context=data)

    def calculate_new_stock(self, request, data):
        stock = {}
        stock['status'] = 1
        prev_qty = float(data['prev_qty'])
        prev_total_price = float(data['prev_total_price'])
        current_qty = float(data['quantity'])
        current_total_price = float(data['total_price'])

        if data['adjust_type'] == 'increase_quantity':
            stock['new_qty'] = round(prev_qty + current_qty, 2)
            stock['new_total_price'] = round(prev_total_price + current_total_price, 2)
            stock['new_unit_price'] = round(stock['new_total_price'] / stock['new_qty'], 2)
        elif data['adjust_type'] == 'decrease_quantity':
            if prev_qty >= current_qty:
                stock['new_qty'] = round(prev_qty - current_qty, 2)
                if stock['new_qty'] > 0:
                    stock['new_total_price'] = round(prev_total_price - current_total_price, 2)
                    stock['new_unit_price'] = round(stock['new_total_price'] / stock['new_qty'], 2)
                else:
                    stock['new_total_price'] = round(0.0, 2)
                    stock['new_unit_price'] = round(0.0, 2)
            else:
                stock['status'] = 0
        else:
            stock['new_qty'] = round(current_qty, 2)
            stock['new_total_price'] = round(current_total_price, 2)
            stock['new_unit_price'] = round(current_total_price / current_qty, 2)

        return stock

    def save(self, request):
        # create a form instance and populate it with data from the request:
        form = ProductStockForm(request.POST)

        # check whether it's valid:
        if form.is_valid():

            # dump_data = dd(request)
            # return HttpResponse(dump_data)

            try:
                data = {}

                product = request.POST.get('product')
                product_category = request.POST.get('product_category')
                product_sub_category = request.POST.get('product_sub_category')
                product_sub_sub_category = request.POST.get('product_sub_sub_category')

                data['adjust_type'] = request.POST.get('adjust_type')
                data['quantity'] = request.POST.get('quantity')
                data['unit_price'] = request.POST.get('unit_price')
                data['total_price'] = request.POST.get('total_price')
                data['product'] = Products.objects.get_product(product, type='admin')
                data['product_category'] = Product_Categories.objects.get_category(product_category)

                if product_sub_category:
                    data['product_sub_category'] = Product_Sub_Categories.objects.get_sub_category(product_sub_category)
                else:
                    data['product_sub_category'] = None
                if product_sub_sub_category:
                    data['product_sub_sub_category'] = Product_Sub_Sub_Categories.objects.get_sub_sub_category(product_sub_sub_category)
                else:
                    data['product_sub_sub_category'] = None

                stock_availability = Product_Available_Stock.objects.get_stock_product(product, product_category, product_sub_category, product_sub_sub_category, type='stock')

                # dump_data = dd(request, stock_availability)
                # return HttpResponse(dump_data)

                if stock_availability:
                    data['prev_qty'] = stock_availability[0].quantity
                    data['prev_unit_price'] = stock_availability[0].unit_price
                    data['prev_total_price'] = stock_availability[0].total_price

                    stock = self.calculate_new_stock(request, data=data)

                    if stock['status'] == 1:
                        # update product stock
                        product_stock = Product_Available_Stock.objects.update(stock_availability[0].id, data=stock)
                        messages.success(request, 'Stock adjusted successfully!')
                    else:
                        messages.success(request, 'Please provide lass quantity than previous quantity!') 

                else:
                    messages.warning(request, 'Product not available in the stock!')

            except Exception as e:
                raise e

            return redirect('/admin/products/product_available_stock/')

        return redirect('/admin/products/product_available_stock/')

    def get_stock_product(self, request):
        product_id = request.POST.get('product_id')
        category_id = request.POST.get('category_id')
        sub_category_id = request.POST.get('sub_category_id')
        sub_sub_category_id = request.POST.get('sub_sub_category_id')

        stock_product = Product_Available_Stock.objects.get_stock_product(product_id, category_id, sub_category_id, sub_sub_category_id, type='stock')

        stock_product = serializers.serialize('json', stock_product)
        # stock_product = serializers.serialize('python', stock_product)[0]

        return JsonResponse(stock_product, safe=False)

    def get_sub_category(self, request):
        category_id = request.POST.get('category_id')
        sub_categories = Product_Sub_Categories.objects.get_category_wise_sub_category(category_id)

        html = '<option value="">Select An Option</option>'
        for sub_category in sub_categories:
            html +='<option value="'+str(sub_category.id)+'">'+sub_category.name+'</option>'

        return HttpResponse(html)

    def get_sub_sub_category(self, request):
        sub_category_id = request.POST.get('sub_category_id')
        sub_sub_categories = Product_Sub_Sub_Categories.objects.get_sub_category_wise_sub_sub_category(sub_category_id)

        html = '<option value="">Select An Option</option>'
        for sub_sub_category in sub_sub_categories:
            html +='<option value="'+str(sub_sub_category.id)+'">'+sub_sub_category.name+'</option>'

        return HttpResponse(html)
