import os
import time
import requests

from django.urls import path
from django.conf import settings
from django.contrib import admin
from django.http import HttpResponse

from products.models import (
    Product_Categories,
    Product_Sub_Categories,
)


class AdminProductColor(admin.ModelAdmin):

    list_filter  = ('name', 'code',)
    list_display = ('name', 'code',)
