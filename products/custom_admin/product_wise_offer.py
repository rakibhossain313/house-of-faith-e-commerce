import os
import time
import requests
import json

from django.urls import path
from django.conf import settings
from django.http import HttpResponse
from django.contrib import admin, messages
from django.shortcuts import render, redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from app_libs.dump import dd
from products.forms import ProductWiseOfferForm
from products.models import (
    Product_Offers,
    Product_Wise_Offer,
    Products,
    Product_Available_Stock,
)


class AdminProductWiseOffer(admin.ModelAdmin):

    change_list_template = 'admin/offers/list.html'
    change_form_template = 'admin/offers/add.html'

    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            path('save/', self.save, name='save_offer'),
            path('edit/<int:id>/', self.edit, name='edit_offer'),
            path('update/<int:id>/', self.update, name='update_offer'),
            path('delete/', self.delete, name='delete_offer'),
        ]

        return custom_urls + urls

    def changelist_view(self, request, extra_context=None):
        data = extra_context or {}
        
        product_wise_offers = Product_Wise_Offer.objects.get_all_product_wise_offer()
        page = request.GET.get('p', 1)

        # dump_data = dd(request, product_wise_offers)
        # return HttpResponse(dump_data)

        # number of product per page
        paginator = Paginator(product_wise_offers, 50)
        try:
            data['product_wise_offers'] = paginator.page(page)
        except PageNotAnInteger:
            data['product_wise_offers'] = paginator.page(1)
        except EmptyPage:
            data['product_wise_offers'] = paginator.page(paginator.num_pages)
        # end of pagination

        return super().changelist_view(request, extra_context=data)

    def changeform_view(self, request, obj_id, form_url='', extra_context=None):
        data = extra_context or {}
        data['products'] = Products.objects.get_all_product()
        data['product_offers'] = Product_Offers.objects.get_all_offer()

        # dump_data = dd(request, data['products'])
        # return HttpResponse(dump_data)

        return super().changeform_view(request, obj_id, form_url, extra_context=data)

    def save(self, request):
        # create a form instance and populate it with data from the request:
        form = ProductWiseOfferForm(request.POST)

        # check whether it's valid:
        if form.is_valid():

            # dump_data = dd(request)
            # return HttpResponse(dump_data)

            try:
                data = {}
                offer_data = {}

                data['amount'] = request.POST.get('amount')
                products = request.POST.getlist('product[]')
                product_offer = request.POST.get('product_offer')

                if products:
                    product_offer = Product_Offers.objects.get_offer(product_offer)

                    for key, value in enumerate(products):
                        offer_data['product_id'] = products[key]
                        offer_data['product_offer_id'] = product_offer.id
                        product_offer_available = Product_Wise_Offer.objects.get_product_offer_availability(data=offer_data)

                        if not product_offer_available:
                            product = Products.objects.get_product(products[key], type='admin')
                            stock = Product_Available_Stock.objects.get_stock_product(product_id=products[key], type='product_offer')

                            if stock:
                                quantity = stock[0].quantity
                                unit_price = stock[0].unit_price

                                data['product'] = product
                                data['product_offer'] = product_offer
                                data['after_offer_unit_price'] = float(unit_price) - ((float(data['amount']) * float(unit_price)) / 100)
                                data['after_offer_total_price'] = data['after_offer_unit_price'] * quantity

                                # save product wise offer
                                product_wise_offer = Product_Wise_Offer.objects.save(data=data)
                                
                                # update product stock
                                product_stock = Product_Available_Stock.objects.update(stock[0].id, data=data)

                messages.success(request, 'Product wise offer has been saved successfully!')

            except Exception as e:
                raise e

            return redirect('/admin/products/product_wise_offer/')

        return redirect('/admin/products/product_wise_offer/')

    def edit(self, request, id):
        data = {}
        data['opts'] = self.model._meta

        data['products'] = Products.objects.get_all_product()
        data['product_offers'] = Product_Offers.objects.get_all_offer()
        data['edit_product_wise_offer'] = Product_Wise_Offer.objects.get_product_wise_offer(id)

        # dump_data = dd(request, data['edit_product_wise_offer'])
        # return HttpResponse(dump_data)

        template = 'admin/offers/edit.html'

        return render(request, template, data)

    def update(self, request, id):
        # create a form instance and populate it with data from the request:
        form = ProductWiseOfferForm(request.POST)

        # check whether it's valid:
        if form.is_valid():

            # dump_data = dd(request)
            # return HttpResponse(dump_data)

            try:
                data = {}
                offer_data = {}

                offer_amount = request.POST.get('amount')
                products = request.POST.getlist('product[]')
                product_offer = request.POST.get('product_offer')
                prev_product_id = request.POST.get('prev_product_id')

                # delete old product wise offer
                Product_Wise_Offer.objects.delete(id)

                data['amount'] = None
                data['after_offer_unit_price'] = None
                data['after_offer_total_price'] = None

                # get back product stock to previous position
                stock = Product_Available_Stock.objects.get_stock_product(product_id=prev_product_id, type='product_offer')
                if stock:
                    product_stock = Product_Available_Stock.objects.update(stock[0].id, data=data)

                if products:
                    product_offer = Product_Offers.objects.get_offer(product_offer)

                    for key, value in enumerate(products):
                        offer_data['product_id'] = products[key]
                        offer_data['product_offer_id'] = product_offer.id
                        product_offer_available = Product_Wise_Offer.objects.get_product_offer_availability(data=offer_data)

                        if not product_offer_available:
                            product = Products.objects.get_product(products[key], type='admin')
                            stock = Product_Available_Stock.objects.get_stock_product(product_id=products[key], type='product_offer')
                            
                            if stock:
                                quantity = stock[0].quantity
                                unit_price = stock[0].unit_price

                                data['amount'] = offer_amount
                                data['product'] = product
                                data['product_offer'] = product_offer
                                data['after_offer_unit_price'] = float(unit_price) - ((float(data['amount']) * float(unit_price)) / 100)
                                data['after_offer_total_price'] = data['after_offer_unit_price'] * quantity

                                # save product wise offer
                                product_wise_offer = Product_Wise_Offer.objects.save(data=data)
                                
                                # update product stock
                                product_stock = Product_Available_Stock.objects.update(stock[0].id, data=data)

                messages.success(request, 'Product wise offer has been updated successfully!')

            except Exception as e:
                raise e

            return redirect('/admin/products/product_wise_offer/')

        return redirect('/admin/products/product_wise_offer/')

    def delete(self, request):
        offer_id = request.POST.get('offer_id')
        product_id = request.POST.get('product_id')
        
        try:
            # delete product wise offer
            Product_Wise_Offer.objects.delete(offer_id)

            data = {}
            data['amount'] = None
            data['after_offer_unit_price'] = None
            data['after_offer_total_price'] = None

            # update product stock
            stock = Product_Available_Stock.objects.get_stock_product(product_id=product_id, type='product_offer')
            if stock:
                product_stock = Product_Available_Stock.objects.update(stock[0].id, data=data)
            
            return HttpResponse('true')
        except Exception as e:
            raise e

        # return redirect('/admin/products/product_wise_offer/')
