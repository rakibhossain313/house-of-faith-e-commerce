import os
import time
import requests

from django.urls import path
from django.conf import settings
from django.contrib import admin
from django.http import HttpResponse

from products.models import (
    Product_Categories,
    Product_Sub_Categories,
)


class AdminProductSubCategory(admin.ModelAdmin):

    list_filter  = ('product_category', 'name', 'slug')
    list_display = ('product_category', 'name', 'slug')
    prepopulated_fields = {"slug": ("name",)}
