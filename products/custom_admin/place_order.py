import os
import time
import requests
import json

from django.db.models import F
from django.forms import model_to_dict
from django.urls import path
from django.conf import settings
from django.http import HttpResponse
from django.contrib import admin, messages
from django.shortcuts import render, redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from app_libs.dump import dd
from app_libs.file_upload import FileUpload
from products.forms import ProductForm, ProductImageForm
from products.models import (
    Product_Categories,
    Product_Sub_Categories,
    Product_Sub_Sub_Categories,
    Product_Size,
    Product_Wise_Size,
    Product_Color,
    Product_Wise_Color,
    Product_Image,
    Products,
    Product_Available_Stock,
    Order, OrderItem, ShippingAddress
)


class PlaceOrder(admin.ModelAdmin):

    change_list_template = 'admin/orders/list.html'

    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            path('cancel/', self.cancel_order, name='cancel_order'),
            path('cancel/<int:id>/', self.cancel_order, name='cancel_order'),
        ]

        return custom_urls + urls

    def changelist_view(self, request, extra_context=None):
        data = extra_context or {}
        
        place_orders = ShippingAddress.objects.get_all_place_order()
        page = request.GET.get('p', 1)

        # dump_data = dd(request, place_orders[0].order.order_item.annotate(product_title=F('product__title')))
        # return HttpResponse(dump_data)

        # number of product per page
        paginator = Paginator(place_orders, 50)
        try:
            data['place_orders'] = paginator.page(page)
        except PageNotAnInteger:
            data['place_orders'] = paginator.page(1)
        except EmptyPage:
            data['place_orders'] = paginator.page(paginator.num_pages)
        # end of pagination

        return super().changelist_view(request, extra_context=data)

    def cancel_order(self, request, id):
        # return HttpResponse(id)
        
        try:
            # cancel order
            Order.objects.cancel_order(id=id)
            return HttpResponse('true')
        except Exception as e:
            raise e
