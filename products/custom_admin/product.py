import os
import time
import requests
import json

from django.urls import path
from django.conf import settings
from django.http import HttpResponse
from django.contrib import admin, messages
from django.shortcuts import render, redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from app_libs.dump import dd
from app_libs.file_upload import FileUpload
from products.forms import ProductForm, ProductImageForm
from products.models import (
    Product_Categories,
    Product_Sub_Categories,
    Product_Sub_Sub_Categories,
    Product_Size,
    Product_Wise_Size,
    Product_Color,
    Product_Wise_Color,
    Product_Image,
    Products,
    Product_Available_Stock,
)


class AdminProduct(admin.ModelAdmin):

    change_list_template = 'admin/products/list.html'
    change_form_template = 'admin/products/add.html'

    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            path('save/', self.save, name='save_product'),
            path('edit/<int:id>/', self.edit, name='edit_product'),
            path('update/<int:id>/', self.update, name='update_product'),
            path('delete/', self.delete, name='delete_product'),
            path('delete/<int:id>/', self.delete, name='delete_product'),
            path('change-product-stock-status/', self.change_product_stock_status, name='change_product_stock_status'),
            path('get-sub-category/', self.get_sub_category, name='get_sub_category'),
            path('get-sub-sub-category/', self.get_sub_sub_category, name='get_sub_sub_category'),
        ]

        return custom_urls + urls

    def changelist_view(self, request, extra_context=None):
        data = extra_context or {}
        
        product_list = Products.objects.get_all_product(type='admin')
        page = request.GET.get('p', 1)

        # dump_data = dd(request, product_list)
        # return HttpResponse(dump_data)

        # number of product per page
        paginator = Paginator(product_list, 50)
        try:
            data['products'] = paginator.page(page)
        except PageNotAnInteger:
            data['products'] = paginator.page(1)
        except EmptyPage:
            data['products'] = paginator.page(paginator.num_pages)
        # end of pagination

        return super().changelist_view(request, extra_context=data)

    def changeform_view(self, request, obj_id, form_url='', extra_context=None):
        data = extra_context or {}
        data['product_categories'] = Product_Categories.objects.get_all_category()
        data['product_sub_categories'] = Product_Sub_Categories.objects.get_all_sub_categories()
        data['product_sub_sub_categories'] = Product_Sub_Sub_Categories.objects.get_all_sub_sub_categories()
        data['product_sizes'] = Product_Size.objects.get_all_size()
        data['product_colors'] = Product_Color.objects.get_all_color()

        return super().changeform_view(request, obj_id, form_url, extra_context=data)

    def save(self, request):
        # create a form instance and populate it with data from the request:
        form = ProductForm(request.POST)

        # check whether it's valid:
        if form.is_valid():

            # dump_data = dd(request)
            # return HttpResponse(dump_data)

            try:
                data = {}

                sizes = request.POST.getlist('size[]')
                colors = request.POST.getlist('color[]')
                images = request.FILES.getlist('image[]')
                product_category = request.POST.get('product_category')
                product_sub_category = request.POST.get('product_sub_category')
                product_sub_sub_category = request.POST.get('product_sub_sub_category')

                data['title'] = request.POST.get('title')
                data['slug'] = request.POST.get('slug')
                data['status'] = request.POST.get('status')
                if data['status']:
                    data['status'] = True
                else:
                    data['status'] = False
                data['quantity'] = request.POST.get('quantity')
                data['unit_price'] = request.POST.get('unit_price')
                data['total_price'] = request.POST.get('total_price')
                data['description'] = request.POST.get('description')
                data['product_category'] = Product_Categories.objects.get_category(product_category)
                data['product_category_slug'] = data['product_category'].slug
                if product_sub_category:
                    data['product_sub_category'] = Product_Sub_Categories.objects.get_sub_category(product_sub_category)
                    data['product_sub_category_slug'] = data['product_sub_category'].slug
                else:
                    data['product_sub_category'] = None
                    data['product_sub_category_slug'] = None
                if product_sub_sub_category:
                    data['product_sub_sub_category'] = Product_Sub_Sub_Categories.objects.get_sub_sub_category(product_sub_sub_category)
                    data['product_sub_sub_category_slug'] = data['product_sub_sub_category'].slug
                else:
                    data['product_sub_sub_category'] = None
                    data['product_sub_sub_category_slug'] = None

                product_availability = Products.objects.get_product_by_slug(data['slug'], type='admin')

                if not product_availability:

                    # save product
                    product = Products.objects.save(data=data)
                    data['product'] = Products.objects.get_product(product.id, type='admin')

                    if product:

                        if sizes:
                            # save many-to-many product wise size
                            product_wise_size = Product_Wise_Size.objects.save()
                            for key, value in enumerate(sizes):
                                data['product_size'] = Product_Size.objects.get_product_size(sizes[key])

                                # save product wise size
                                product_wise_size.product.add(data['product'])
                                product_wise_size.product_size.add(data['product_size'])

                        if colors:
                            # save many-to-many product wise color
                            product_wise_color = Product_Wise_Color.objects.save()
                            for key, value in enumerate(colors):
                                data['product_color'] = Product_Color.objects.get_product_color(colors[key])

                                # save product wise color
                                product_wise_color.product.add(data['product'])
                                product_wise_color.product_color.add(data['product_color'])

                        if images:

                            files = images
                            for key, value in enumerate(files):
                                data['file_path'] = FileUpload.upload(request, 'products', files[key])
                                # data['file_path'] = FileUpload.resizeUpload(request, 'products', files[key])

                                # save product image
                                product_image = Product_Image.objects.save(data=data)

                        # save product stock
                        if data['total_price']:
                            product_stock = Product_Available_Stock.objects.save(data=data)

                    messages.success(request, 'Product has been saved successfully!')

                else:
                    messages.warning(request, 'Product already available in the stock!')

            except Exception as e:
                raise e

            return redirect('/admin/products/products/')

        return redirect('/admin/products/products/')

    def edit(self, request, id):
        data = {}
        data['opts'] = self.model._meta
        data['product'] = Products.objects.get_product(id, type='admin')
        data['product_categories'] = Product_Categories.objects.get_all_category()
        data['product_sub_categories'] = Product_Sub_Categories.objects.get_all_sub_categories()
        data['product_sub_sub_categories'] = Product_Sub_Sub_Categories.objects.get_all_sub_sub_categories()
        data['product_sizes'] = Product_Size.objects.get_all_size()
        data['product_colors'] = Product_Color.objects.get_all_color()
        data['product_images'] = Product_Image.objects.get_product_all_image(data['product'].id)

        product_wise_sizes = Product_Wise_Size.objects.get_product_wise_size(data['product'].id)
        if product_wise_sizes:
            data['product_wise_sizes'] = [product_wise_size.id for product_wise_size in product_wise_sizes.product_size.all()]

        product_wise_colors = Product_Wise_Color.objects.get_product_wise_color(data['product'].id)
        if product_wise_colors:
            data['product_wise_colors'] = [product_wise_color.id for product_wise_color in product_wise_colors.product_color.all()]

        template = 'admin/products/edit.html'

        return render(request, template, data)

    def update(self, request, id):
        # create a form instance and populate it with data from the request:
        form = ProductForm(request.POST)

        # check whether it's valid:
        if form.is_valid():

            # dump_data = dd(request)
            # return HttpResponse(dump_data)

            try:
                data = {}

                sizes = request.POST.getlist('size[]')
                colors = request.POST.getlist('color[]')
                images = request.FILES.getlist('image[]')
                edit_images = request.POST.getlist('edit_image[]')
                product_category = request.POST.get('product_category')
                product_sub_category = request.POST.get('product_sub_category')
                product_sub_sub_category = request.POST.get('product_sub_sub_category')

                data['title'] = request.POST.get('title')
                data['slug'] = request.POST.get('slug')
                data['status'] = request.POST.get('status')
                if data['status']:
                    data['status'] = True
                else:
                    data['status'] = False
                data['description'] = request.POST.get('description')
                data['product_category'] = Product_Categories.objects.get_category(product_category)
                data['product_category_slug'] = data['product_category'].slug
                if product_sub_category:
                    data['product_sub_category'] = Product_Sub_Categories.objects.get_sub_category(product_sub_category)
                    data['product_sub_category_slug'] = data['product_sub_category'].slug
                else:
                    data['product_sub_category'] = None
                    data['product_sub_category_slug'] = None
                if product_sub_sub_category:
                    data['product_sub_sub_category'] = Product_Sub_Sub_Categories.objects.get_sub_sub_category(product_sub_sub_category)
                    data['product_sub_sub_category_slug'] = data['product_sub_sub_category'].slug
                else:
                    data['product_sub_sub_category'] = None
                    data['product_sub_sub_category_slug'] = None

                # update product
                product = Products.objects.update(id=id, data=data)
                data['product'] = Products.objects.get_product(product.id, type='admin')

                if product:

                    if sizes:
                        # delete old many-to-many product wise size
                        Product_Wise_Size.objects.delete(data['product'].id)

                        # save new many-to-many product wise size
                        product_wise_size = Product_Wise_Size.objects.save()
                        for key, value in enumerate(sizes):
                            data['product_size'] = Product_Size.objects.get_product_size(sizes[key])

                            # save product wise size
                            product_wise_size.product.add(data['product'])
                            product_wise_size.product_size.add(data['product_size'])
                    else:
                        # delete old many-to-many product wise size
                        Product_Wise_Size.objects.delete(data['product'].id)

                    if colors:
                        # delete old many-to-many product wise color
                        Product_Wise_Color.objects.delete(data['product'].id)

                        # save new many-to-many product wise color
                        product_wise_color = Product_Wise_Color.objects.save()
                        for key, value in enumerate(colors):
                            data['product_color'] = Product_Color.objects.get_product_color(colors[key])

                            # save product wise color
                            product_wise_color.product.add(data['product'])
                            product_wise_color.product_color.add(data['product_color'])
                    else:
                        # delete old many-to-many product wise color
                        Product_Wise_Color.objects.delete(data['product'].id)

                    if edit_images:

                        for key, value in enumerate(edit_images):
                            old_product_image = Product_Image.objects.get_product_image(edit_images[key])
                            if old_product_image:
                                # delete product old images
                                Product_Image.objects.delete(old_product_image.id)
                                delete_file = settings.BASE_DIR+old_product_image.image_path
                                if os.path.isfile(delete_file):
                                    os.remove(delete_file)

                    if images:

                        files = images
                        for key, value in enumerate(files):
                            data['file_path'] = FileUpload.upload(request, 'products', files[key])
                            # data['file_path'] = FileUpload.resizeUpload(request, 'products', files[key])

                            # save new product image
                            product_image = Product_Image.objects.save(data=data)

                    # update product stock
                    stock_availability = Product_Available_Stock.objects.get_stock_product(product.id, product_category, product_sub_category, product_sub_sub_category, type='product')
                    if stock_availability:
                        product_stock = Product_Available_Stock.objects.update(stock_availability[0].id, data=data)

                messages.success(request, 'Product has been updated successfully!')

            except Exception as e:
                raise e

            return redirect('/admin/products/products/')

        return redirect('/admin/products/products/')

    def delete(self, request, id):
        # return HttpResponse(id)
        
        try:
            # delete many-to-many product wise size
            Product_Wise_Size.objects.delete(id)

            # delete many-to-many product wise color
            Product_Wise_Color.objects.delete(id)

            # delete product images
            product_images = Product_Image.objects.get_product_all_image(id)
            if product_images:
                for product_image in product_images:
                    Product_Image.objects.delete(product_image.id)
                    delete_file = settings.BASE_DIR+product_image.image_path
                    if os.path.isfile(delete_file):
                        os.remove(delete_file)

            # delete product
            Products.objects.delete(id=id)
            
            return HttpResponse('true')
        except Exception as e:
            raise e

        # return redirect('/admin/products/products/')

    def change_product_stock_status(self, request):
        id = request.POST.get('id')
        status = json.loads(request.POST.get('status'))

        try:
            # change product stock status
            Products.objects.change_product_stock_status(id=id, status=status)
            return HttpResponse('true')
        except Exception as e:
            raise e

    def get_sub_category(self, request):
        category_id = request.POST.get('category_id')
        sub_categories = Product_Sub_Categories.objects.get_category_wise_sub_category(category_id)

        html = '<option value="">Select An Option</option>';
        for sub_category in sub_categories:
            html +='<option value="'+str(sub_category.id)+'">'+sub_category.name+'</option>'

        return HttpResponse(html)

    def get_sub_sub_category(self, request):
        sub_category_id = request.POST.get('sub_category_id')
        sub_sub_categories = Product_Sub_Sub_Categories.objects.get_sub_category_wise_sub_sub_category(sub_category_id)

        html = '<option value="">Select An Option</option>';
        for sub_sub_category in sub_sub_categories:
            html +='<option value="'+str(sub_sub_category.id)+'">'+sub_sub_category.name+'</option>'

        return HttpResponse(html)
