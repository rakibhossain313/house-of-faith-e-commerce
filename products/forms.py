from django import forms
from .models import (
	Products, 
	Product_Image, 
	Product_Available_Stock, 
	Product_Wise_Offer,
)


class ProductForm(forms.ModelForm):
	class Meta:
		model = Products
		fields = ['title', 'product_category', 'status', 'description']


class ProductImageForm(forms.ModelForm):
	class Meta:
		model = Product_Image
		fields = ['product', 'image_path']


class ProductStockForm(forms.ModelForm):
	class Meta:
		model = Product_Available_Stock
		fields = ['product', 'product_category', 'product_sub_category', 'product_sub_sub_category', 'quantity', 'unit_price', 'total_price']


class ProductWiseOfferForm(forms.ModelForm):
	class Meta:
		model = Product_Wise_Offer
		fields = ['product_offer', 'amount']
