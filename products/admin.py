from django.urls import path
from django.contrib import admin

from .custom_admin.product_category import AdminProductCategory
from .custom_admin.product_sub_category import AdminProductSubCategory
from .custom_admin.product_sub_sub_category import AdminProductSubSubCategory
from .custom_admin.product_available_stock import AdminProductAvailableStock
from .custom_admin.product_wise_offer import AdminProductWiseOffer
from .custom_admin.product_size import AdminProductSize
from .custom_admin.product_color import AdminProductColor
from .custom_admin.product import AdminProduct
from .custom_admin.place_order import PlaceOrder
from .models import (
    Product_Categories, 
    Product_Sub_Categories, 
    Product_Sub_Sub_Categories, 
    Product_Size, 
    Products, 
    Product_Color, 
    Product_Rating, 
    Product_Offers, 
    Product_Wise_Offer, 
    Product_Available_Stock,
    Order
)


admin.site.register(Product_Categories, AdminProductCategory)
admin.site.register(Product_Sub_Categories, AdminProductSubCategory)
admin.site.register(Product_Sub_Sub_Categories, AdminProductSubSubCategory)
admin.site.register(Product_Size, AdminProductSize)
admin.site.register(Product_Color, AdminProductColor)
admin.site.register(Products, AdminProduct)
admin.site.register(Product_Rating)
admin.site.register(Product_Offers)
# admin.site.register(Product_Wise_Offer)
admin.site.register(Product_Wise_Offer, AdminProductWiseOffer)
admin.site.register(Product_Available_Stock, AdminProductAvailableStock)
admin.site.register(Order, PlaceOrder)
