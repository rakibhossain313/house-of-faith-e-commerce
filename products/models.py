from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext_lazy as _
from ckeditor.fields import RichTextField


from accounts.models import User
from .model_managers import (
	ProductCategoryManager,
	ProductSubCategoryManager,
	ProductSubSubCategoryManager,
	ProductSizeManager,
	ProductWiseSizeManager,
	ProductColorManager,
	ProductWiseColorManager,
	ProductOfferManager,
	ProductWiseOfferManager,
	ProductManager,
	ProductAvailableStockManager,
	ProductImageManager,
	OrderManager,
	OrderItemManager,
	ShippingAddressManager
)


class Product_Categories(models.Model):
	id = models.AutoField(primary_key=True, editable=False)
	name = models.CharField(max_length=50)
	slug = models.SlugField(max_length=250, unique=True)
	categoryImage = models.ImageField(upload_to='images/category/')

	objects = ProductCategoryManager()

	def __str__(self):
		return self.name

	class Meta:
		verbose_name = _('Category')
		verbose_name_plural = _('       Categories')


class Product_Sub_Categories(models.Model):
	id = models.AutoField(primary_key=True, editable=False)
	product_category = models.ForeignKey(Product_Categories, on_delete=models.CASCADE, related_name='product_sub_category')
	name = models.CharField(max_length=50)
	slug = models.SlugField(max_length=250, unique=True)

	objects = ProductSubCategoryManager()

	def __str__(self):
		return self.name

	class Meta:
		verbose_name = _('Sub Category')
		verbose_name_plural = _('      Sub Categories')


class Product_Sub_Sub_Categories(models.Model):
	id = models.AutoField(primary_key=True, editable=False)
	product_category = models.ForeignKey(Product_Categories, on_delete=models.CASCADE, related_name='product_category_sub_category')
	product_sub_category = models.ForeignKey(Product_Sub_Categories, on_delete=models.CASCADE, related_name='product_sub_sub_category')
	name = models.CharField(max_length=50)
	slug = models.SlugField(max_length=250, unique=True)

	objects = ProductSubSubCategoryManager()

	def __str__(self):
		return self.name

	class Meta:
		verbose_name = _('Sub Sub Category')
		verbose_name_plural = _('     Sub Sub Categories')


class Product_Size(models.Model):
	id = models.AutoField(primary_key=True, editable=False)
	name = models.CharField(max_length=20)

	objects = ProductSizeManager()

	def __str__(self):
		return self.name

	class Meta:
		verbose_name = _('    Size')


class Product_Color(models.Model):
	id = models.AutoField(primary_key=True, editable=False)
	name = models.CharField(max_length=20)
	code = models.CharField(max_length=20)

	objects = ProductColorManager()

	def __str__(self):
		return self.name

	class Meta:
		verbose_name = _('   Color')


class Product_Offers(models.Model):
	id = models.AutoField(primary_key=True, editable=False)
	name = models.CharField(max_length=100)

	objects = ProductOfferManager()

	def __str__(self):
		return self.name

	def firstWord(self):
		first, *_ = self.name.split(maxsplit=1)
		return first

	class Meta:
		verbose_name = _('   Offer')


class Products(models.Model):
	id = models.AutoField(primary_key=True, editable=False)
	title = models.CharField(max_length=250)
	slug = models.SlugField(max_length=250, unique=True)
	product_category = models.ForeignKey(Product_Categories, on_delete=models.CASCADE, related_name='category_product')
	product_category_slug = models.SlugField(max_length=200)
	product_sub_category = models.ForeignKey(Product_Sub_Categories, on_delete=models.SET_NULL, blank=True, null=True, related_name='sub_category_product')
	product_sub_category_slug = models.SlugField(max_length=200, blank=True, null=True)
	product_sub_sub_category = models.ForeignKey(Product_Sub_Sub_Categories, on_delete=models.SET_NULL, blank=True, null=True, related_name='sub_sub_category_product')
	product_sub_sub_category_slug = models.SlugField(max_length=200, blank=True, null=True)
	status = models.BooleanField()
	description = RichTextField()

	objects = ProductManager()

	def __str__(self):
		return self.title

	class Meta:
		verbose_name = _('  Product')


class Product_Image(models.Model):
	id = models.AutoField(primary_key=True, editable=False)
	product = models.ForeignKey(Products, on_delete=models.CASCADE, related_name='product_image')
	image_path = models.CharField(max_length=100, null=True)

	objects = ProductImageManager()

	def __str__(self):
		return self.product


class Product_Rating(models.Model):
	id = models.AutoField(primary_key=True, editable=False)
	product = models.ForeignKey(Products, on_delete=models.CASCADE, related_name='product_rating')
	user = models.ForeignKey(User, null=True, on_delete=models.SET_NULL, related_name='product_rate_user')
	rating = models.IntegerField(null=True)
	review = models.TextField(blank=True, null=True)

	def __str__(self):
		return self.rating

	class Meta:
		verbose_name = _('Product Rating')


class Product_Wise_Size(models.Model):
	id = models.AutoField(primary_key=True, editable=False)
	product = models.ManyToManyField(Products, related_name='product_wise_size')
	product_size = models.ManyToManyField(Product_Size)

	objects = ProductWiseSizeManager()

	def __str__(self):
		return self.product_size


class Product_Wise_Color(models.Model):
	id = models.AutoField(primary_key=True, editable=False)
	product = models.ManyToManyField(Products)
	product_color = models.ManyToManyField(Product_Color)

	objects = ProductWiseColorManager()

	def __str__(self):
		return self.product_color


class Product_Wise_Offer(models.Model):
	id = models.AutoField(primary_key=True, editable=False)
	product = models.ForeignKey(Products, on_delete=models.CASCADE, related_name='product_offer')
	product_offer = models.ForeignKey(Product_Offers, on_delete=models.CASCADE)
	amount = models.DecimalField(max_digits=20, decimal_places=2, null=True)

	objects = ProductWiseOfferManager()

	def __str__(self):
		return self.product_offer

	class Meta:
		verbose_name = _('Product Wise Offer')
		verbose_name_plural = _('Product Wise Offers')


class Product_Available_Stock(models.Model):
	id = models.AutoField(primary_key=True, editable=False)
	product_category = models.ForeignKey(Product_Categories, on_delete=models.CASCADE, related_name='stock_category')
	product_sub_category = models.ForeignKey(Product_Sub_Categories, blank=True, null=True, on_delete=models.SET_NULL, related_name='stock_sub_category')
	product_sub_sub_category = models.ForeignKey(Product_Sub_Sub_Categories, blank=True, null=True, on_delete=models.SET_NULL, related_name='stock_sub_sub_category')
	product = models.ForeignKey(Products, on_delete=models.CASCADE, related_name='stock_product')
	offer_amount = models.DecimalField(max_digits=20, decimal_places=2, blank=True, null=True)
	quantity = models.IntegerField(default=0, null=True)
	unit_price = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	total_price = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	after_offer_unit_price = models.DecimalField(max_digits=20, decimal_places=2, blank=True, null=True)
	after_offer_total_price = models.DecimalField(max_digits=20, decimal_places=2, blank=True, null=True)

	objects = ProductAvailableStockManager()

	def __str__(self):
		return str(self.product)

	class Meta:
		verbose_name = _(' Stock Product')


class Order(models.Model):
	id = models.AutoField(primary_key=True, editable=False)
	customer = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, related_name='customer_order')
	is_complete = models.BooleanField(default=False)
	created_at = models.DateTimeField(auto_now_add=True)

	objects = OrderManager()

	def __str__(self):
		return str(self.id)

	@property
	def get_cart_total(self):
		order_items = self.orderitem_set.all()
		total = sum([item.get_total for item in order_items])
		return total

	@property
	def get_cart_items(self):
		order_items = self.orderitem_set.all()
		total = sum([item.quantity for item in order_items])
		return total


class OrderItem(models.Model):
	id = models.AutoField(primary_key=True, editable=False)
	order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='order_item')
	product = models.ForeignKey(Products, on_delete=models.SET_NULL, null=True, blank=True, related_name='order_product')
	product_size = models.ForeignKey(Product_Size, on_delete=models.SET_NULL, null=True, blank=True, related_name='order_product_size')
	quantity = models.IntegerField(default=0, null=True)
	unit_price = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	created_at = models.DateTimeField(auto_now_add=True)

	objects = OrderItemManager()

	@property
	def get_total(self):
		return str(self.unit_price * self.quantity)


class ShippingAddress(models.Model):
	id = models.AutoField(primary_key=True, editable=False)
	customer = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name='place_order_customer')
	order = models.ForeignKey(Order, on_delete=models.SET_NULL, null=True, related_name='place_order')
	email = models.EmailField(null=True, blank=True)
	mobile_number = models.CharField(max_length=20, null=False)
	alternative_mobile_number = models.CharField(max_length=20, blank=True, null=True)
	address = models.CharField(max_length=200, null=False)
	remarks = RichTextField(blank=True, null=True)
	created_at = models.DateTimeField(auto_now_add=True)

	objects = ShippingAddressManager()

	def __str__(self):
		return self.address
