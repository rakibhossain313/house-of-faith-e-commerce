from django.db import models


# FAQ Models Managers
class FaqManager(models.Manager):
	# get all faqs
	def all_faq(self):
		return self.filter().order_by('id')


# Shop Opening Time Models Managers
class SOTManager(models.Manager):
	def all_sot(self):
		return self.filter().order_by('id')


# Company Info Model Manager
class CIManager(models.Manager):
	# get company info
	def all_ci(self):
		return self.filter()


# Owner and Team Models Manager
class OATManager(models.Manager):
	def all_oat(self):
		return self.filter()


# Default Slider Model Manager
class D_SlideManager(models.Manager):
	def all_D_Slide(self):
		return self.filter().order_by('id')


# Carousel Slider Model Manager
class C_SlideManager(models.Manager):
	def all_C_Slide(self):
		return self.filter().order_by('id')


# class PNLManager(models.Manager):
# 	def pnl(self):
# 		return self.get()

class HomeCredentials(models.Manager):
	def all_home_cre(self):
		return self.filter()
