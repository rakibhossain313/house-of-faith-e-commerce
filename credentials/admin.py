from django.contrib import admin
from .models import(
    Faq,
    ShopOpeningTime,
    CompanyInfo,
    OwnerAndTeam,
    Default_Slide,
    # PopupNewsletter,
    Carousel_Slide,
    Home_Credentials,
)
from django.db import models
from django.forms import Textarea

# class PopupAdmin(admin.ModelAdmin):
#     list_display = (
#         'offer_Range',
#     )

class SliderAdmin(admin.ModelAdmin):
    list_display = (
        'id',
    )

class FaqAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'questions',
    )

    list_display_links = (
        'questions',
    )

    search_fields = [
        'questions',
        'answers',
    ]

class OwnerAndTeamAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': Textarea(
            attrs = {
                'rows': 3,
                'cols': 5,
                'style': 'height: 8em; width: 40em;',
            })
        },
    }

    list_display = (
        'name',
        'designation',
    )

    list_display_links = (
        'name',
    )

class ContactAbout(admin.ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': Textarea(
            attrs = {
                'rows': 3,
                'cols': 5,
                'style': 'height: 7em; width: 40em;',
            })
        },
    }

# class CompanyInfoAdmin(admin.ModelAdmin):
#     list_display = (
#         'name',
#     )

admin.site.register(Faq, FaqAdmin)
admin.site.register(ShopOpeningTime)
admin.site.register(CompanyInfo, ContactAbout)
admin.site.register(OwnerAndTeam, OwnerAndTeamAdmin)
admin.site.register(Default_Slide, SliderAdmin)
# admin.site.register(PopupNewsletter, PopupAdmin)
admin.site.register(Carousel_Slide)
admin.site.register(Home_Credentials)
