from django.db import models
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from django.utils.translation import ugettext_lazy as _

from .managers import(
    FaqManager,
    SOTManager,
    CIManager,
    OATManager,
    D_SlideManager,
    C_SlideManager,
    # PNLManager,
    HomeCredentials,
)

class Faq(models.Model):
    questions = models.CharField(max_length=250)
    answers = RichTextField()

    objects = FaqManager()

    def __str__(self):
        return self.questions

    class Meta:
        verbose_name = _(' FAQ')


class ShopOpeningTime(models.Model):
    DAYS_OF_WEEK = (
        ('Monday', 'Monday'),
        ('Tuesday', 'Tuesday'),
        ('Wednesday', 'Wednesday'),
        ('Thursday', 'Thursday'),
        ('Friday', 'Friday'),
        ('Saturday', 'Saturday'),
        ('Sunday', 'Sunday'),
	)

    day = models.CharField(max_length=10, choices=DAYS_OF_WEEK)
    sameday = models.CharField(max_length=10, choices=DAYS_OF_WEEK, blank=True)
    opentime = models.TimeField()
    closetime = models.TimeField()

    objects = SOTManager()

    def __str__(self):
        return self.day

    class Meta:
        verbose_name = _('  Opening Hour')


class CompanyInfo(models.Model):
    images = models.ImageField(upload_to='images/')
    address = models.CharField(max_length=40)
    phone = models.CharField(max_length=20)
    email = models.EmailField(max_length=50)
    about_Contact_Page = models.TextField()
    aboutCompany = RichTextUploadingField()

    objects	= CIManager()

    # def __str__(self):
    #     return self.name.title()

    def aboutFirst(self):
        return self.aboutCompany[:360]

    def aboutScond(self):
        return self.aboutCompany[360:]

    class Meta:
        verbose_name_plural = _('    About Us')


class OwnerAndTeam(models.Model):
    images = models.ImageField(upload_to='images/')
    name = models.CharField(max_length=25)
    designation = models.CharField(max_length=20)
    bioAP = models.TextField()

    objects = OATManager()

    class Meta:
        verbose_name_plural = _('   Owner & Team')


class Default_Slide(models.Model):
    image = models.ImageField(upload_to='images/slide/')
    PNG = models.ImageField(upload_to='images/slide/')
    url = models.URLField(max_length=300)

    objects = D_SlideManager()

    class Meta:
        verbose_name = _('      Default Slider')


class Carousel_Slide(models.Model):
    image = models.ImageField(upload_to='images/slide/')
    url = models.URLField(max_length=300)

    objects = C_SlideManager()

    class Meta:
        verbose_name = _('     Carousel Slider')


# class PopupNewsletter(models.Model):
#     offer_Range = models.CharField(max_length=15)
#     product_Category_Name = models.CharField(max_length=15)
#     product_PNG_Image = models.ImageField(upload_to='images/popup/')
#
#     objects = PNLManager()

class Home_Credentials(models.Model):
    welcome_Message = models.CharField(max_length=30)
    aboutCompany = RichTextUploadingField()
    image_One = models.ImageField(upload_to='images/homeCredentials/')
    image_Two = models.ImageField(upload_to='images/homeCredentials/')
    url = models.URLField(max_length=300)

    objects = HomeCredentials()

    def __str__(self):
        return self.welcome_Message

    class Meta:
        verbose_name = _('       Home Credentials')
